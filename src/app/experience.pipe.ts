import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'experience'
})
export class ExperiencePipe implements PipeTransform {

  transform(value: Date): string {
    let now = new Date();
    let experience = this.calculateExperience(value, now);
    return experience;
  }

  calculateExperience(doj: Date, now: Date): string {
    let years = now.getFullYear() - doj.getFullYear();
    let months = now.getMonth() - doj.getMonth();

    if (months < 0 || (months === 0 && now.getDate() < doj.getDate())) {
      years--;
      months += 12;
    }

    return `${years}`;
  } 
}

