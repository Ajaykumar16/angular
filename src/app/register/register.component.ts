import { Component } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  employee = {
    empId: '',
    empName: '',
    salary: '',
    gender: '',
    doj: '',
    country: '',
    emailId: '',
    password: ''
  };

  

  
  onSubmit() {
    console.log('Form submitted:', this.employee);
  }
}