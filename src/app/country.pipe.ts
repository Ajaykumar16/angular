import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'country'
})
export class CountryPipe implements PipeTransform {

  transform(country:string): string {
    const countryMapping:{[key:string]:string}={
      'united states':'USA',
      'india':'IND' ,
      'newzeland':'NZ' , 
      'united kingdom':'UK'
    };

    const lowerCaseValue=country.toLowerCase();
  if(countryMapping[lowerCaseValue]){
    return countryMapping[lowerCaseValue];
  }
  return country;
  }
  
  

}
