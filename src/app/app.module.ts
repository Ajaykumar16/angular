import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { ShowEmployeesComponent } from './show-employees/show-employees.component';
import { GenderPipe } from './gender.pipe';
import { ExperiencePipe } from './experience.pipe';
import { CountryPipe } from './country.pipe';
import { ProductComponent } from './product/product.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ShowEmployeesComponent,
    GenderPipe,
    ExperiencePipe,
    CountryPipe,
    ProductComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [
    provideClientHydration()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
