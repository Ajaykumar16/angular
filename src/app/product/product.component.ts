import { Component } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent {
  clothingItems = [
    {
      id: 1,
      name: 'Men\'s T-Shirt',
      description: 'Comfortable cotton T-shirt for men.',
      price: 35.99,
      imageUrl: '../../../assets/133.jpeg'
    },
    {
      id: 2,
      name: 'Women\'s Sarees',
      description: 'Stylish and durable jeans for women.',
      price: 23.99,
      imageUrl: '../../../assets/144.jpeg'
    },
    {
      id: 3,
      name: 'Men\'s Hoodie',
      description: 'Warm and cozy hoodie for men.',
      price: 19.99,
      imageUrl: '../../../assets/32.jpg'
    },
  ];
}
