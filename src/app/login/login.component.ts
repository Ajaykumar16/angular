import { Component } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loginForm: any = {}; 

  constructor() {}

  login() {
    console.log(this.loginForm); 
    if (this.loginForm.email === "admin@ts.com" && this.loginForm.password === "Admin@123") {
      alert("Login Successful");
      console.log("EmailId:" + this.loginForm.email);
      console.log("Password:" + this.loginForm.password);
    } else {
      alert("Login Failed");
    }
  }

  private validateForm(): boolean {
    return this.loginForm.email.trim() !== '' && this.loginForm.password.trim() !== '';
  }
}