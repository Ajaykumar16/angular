import { Component } from '@angular/core';

@Component({
  selector: 'app-show-employees',
  templateUrl: './show-employees.component.html',
  styleUrl: './show-employees.component.css'
})
export class ShowEmployeesComponent {
  employees: any[];

  constructor() {
    this.employees = [
      {
        id: 1,
        name: 'Ajay',
        salary: 100000,
        position: 'Developer',
        gender: 'Male',
        email: 'ajay4@gmail.com',
        doj:new Date('01/18/2016'),
        country: 'india'
        
        
      },
      {
        id: 2,
        name: 'Sai',
        salary: 120000,
        position: 'Tester',
        gender: 'Male',
        email: 'sai55@gmail.com',
        doj:new Date('02/24/2013'),
        country: 'usa'
       
        
      },
      {
        id: 3,
        name: 'Vani',
        salary: 290000,
        position: 'Manager',
        gender: 'Female',
        email: 'vani41@gmail.com',
        doj:new Date('05/07/2022'),
        country: 'uk'
        
        
      },


      {
        id: 4,
        name: 'Akshay',
        salary: 250000,
        position: 'Client',
        gender: 'Male',
        email: 'akshay1@gmail.com',
        doj:new Date('03/03/2010'),
        country: 'nz'
        
        
      },

      
    ];
  }
  
}

